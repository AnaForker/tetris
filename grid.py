import pygame


class Grid:

    def __init__(self, columns, rows, block_dimensions, screen_res, background_path):
        self.score = 0
        self.game_over = False

        self.block_width = block_dimensions[0]
        self.block_height = block_dimensions[1]

        self.area_width = columns * self.block_width
        self.area_height = rows * self.block_height

        # top left corner of game area
        self.min_coord = ((screen_res[0] - self.area_width - 80),
                          (screen_res[1] - self.area_height) / 2)
        # bottom right corner of game area
        self.max_coord = (self.min_coord[0] + self.area_width,
                          self.min_coord[1] + self.area_height)

        # center coord of game area (tetromino start coord)
        self.center_coord = (self.min_coord[0] + (columns // 2) * self.block_width,
                             self.min_coord[1] + self.block_height)

        self.columns = columns
        self.rows = rows
        # rows x columns grid
        self.grid = [[-1 for i in range(self.columns)] for j in range(self.rows)]

        self.background = pygame.image.load(background_path).convert()
        self.background = pygame.transform.scale(self.background, (self.area_width, self.area_height))
        self.background.set_alpha(200)

    def overlap(self, tetromino_coords):
        """
        :param tetromino_coords: list of coordinates (x, y)
        :return: True if blocks overlap
        """
        indexes_list = self.convert_coords(tetromino_coords)
        for row_index, column_index in indexes_list:
            if row_index >= self.rows or self.grid[row_index][column_index] >= 0:
                return True
        return False

    def is_out_of_bounds(self, tetromino_coords):
        """
        :param tetromino_coords: list of coordinates (x, y)
        :return: True if a block is out of bounds
        """
        for x, y in tetromino_coords:
            if x > self.max_coord[0] - self.block_width or x < self.min_coord[0]:
                return True
        return False

    def is_game_over(self):
        return self.game_over

    def convert_coords(self, coords):
        """
        Convert coordinates to grid indexes.
        :param coords: list of coordinates (x, y)
        :return: list of indexes (row_index, column_index)
        """
        indexes_list = []
        for coord in coords:
            column_index = int((coord[0] - self.min_coord[0]) // self.block_width)
            row_index = int((coord[1] - self.min_coord[1]) // self.block_height)
            indexes_list.append((row_index, column_index))
        return indexes_list

    def convert_indexes(self, indexes):
        """
        Convert grid indexes to coordinates
        :param indexes: list of indexes (row_index, column_index)
        :return: list of coordinates (x, y)
        """
        coords_list = []
        for index in indexes:
            x = int(index[1] * self.block_width + self.min_coord[0])
            y = int(index[0] * self.block_height + self.min_coord[1])
            coords_list.append((x, y))
        return coords_list

    def update(self, coords, color_index):
        """
        Converts coordinates to grid indexes and
        assigns color_index to the corresponding cells.
        :param coords: list of coordinates (x, y)
        :param color_index: tetromino color index
        """
        indexes_list = self.convert_coords(coords)
        for row_index, column_index in indexes_list:
            if row_index >= 0 and column_index >= 0:
                self.grid[row_index][column_index] = color_index

        # search for full rows
        for row_index, column_index in indexes_list:
            # check game over
            if row_index == 0:  # there is a block at the first row
                self.game_over = True

            full_row = True
            for j in range(self.columns):
                if self.grid[row_index][j] == -1:  # cell is empty
                    full_row = False
                    break

            # delete the row if it is full
            if full_row:
                del self.grid[row_index]
                self.score += 1
                # insert a new line at the beginning of the grid
                self.grid.insert(0, [-1 for i in range(self.columns)])

    def get_assist_coords(self, tetromino_coords):
        """
        :param tetromino_coords: list of coordinates (x, y)
        :return: coordinates of the assist blocks
        """
        indexes_list = self.convert_coords(tetromino_coords)
        bottom = False
        overlap = False

        # tetromino reached bottom
        for row_index, column_index in indexes_list:
            if row_index >= self.rows - 1:
                return tetromino_coords

        while not bottom and not overlap:
            # for every block
            for i in range(len(indexes_list)):
                row_index, column_index = indexes_list[i]
                indexes_list[i] = (row_index + 1, column_index)
                # check next row
                if self.grid[row_index + 1][column_index] >= 0:
                    overlap = True
                elif row_index + 1 >= self.rows - 1:
                    bottom = True

        if overlap:
            i = 0
            for row_index, column_index in indexes_list:
                indexes_list[i] = (row_index - 1, column_index)
                i += 1
        return self.convert_indexes(indexes_list)

    def show(self, screen, color_blocks):
        """
        Display background and all blocks.
        :param screen: screen surface
        :param color_blocks: block sprites tuple
        """
        screen.blit(self.background, self.min_coord)
        for i in range(self.rows):
            for j in range(self.columns):
                if self.grid[i][j] >= 0:  # if cell isn't empty (empty -> -1)
                    # cell value is color index
                    color_index = self.grid[i][j]
                    coord_x = self.min_coord[0] + j * self.block_width
                    coord_y = self.min_coord[1] + i * self.block_height
                    screen.blit(color_blocks[color_index], (coord_x, coord_y))

    def display_message(self, screen, font, color, message):
        text_surface = font.render(str(message), True, color).convert_alpha()
        text_x = self.min_coord[0] + (self.area_width - text_surface.get_width()) / 2
        text_y = (self.min_coord[1] + self.area_height) / 2
        screen.blit(text_surface, (text_x, text_y))
        pygame.display.flip()

    def get_score(self):
        return self.score

    def get_center_coord(self):
        return self.center_coord
